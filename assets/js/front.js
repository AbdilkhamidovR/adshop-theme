/*
* Scripts for frontend
 */

(function ($) {
	"use strict";
	
	$(document).ready(function(){
	})
	.on('click', '.site-header .menu-toggle', function(e){
		var mobile_navigation = $(this).closest(".site-header").find(".mobile-navigation");
		$(mobile_navigation).toggleClass("toggled");
	})
	;
	
	
	$(window).on('load', function(){});
	
	
	/*
	 * functions
	 */
	
})(jQuery);