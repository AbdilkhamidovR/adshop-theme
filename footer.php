<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="col-full d-flex jc-sb">
            <div class="adsh-col-1-3">
                <h3><?php _e("Payment methods", "adsh"); ?></h3>
                <ul class="payments-list ls-n mr-0">
                    <li class="payment-item paypal"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo_paypal.svg" width="165" height="40" alt=""></li>
                    <li class="payment-item sepa"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo_sepa.svg" width="105" height="29" alt=""></li>
                    <li class="payment-item visa"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo_visa.svg" width="119" height="39" alt=""></li>
                    <li class="payment-item mastercard"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo_mastercard.svg" width="140" height="84" alt=""></li>
                </ul>
            </div>
            <div class="adsh-col-1-3 ta-c">
                <h3><?php _e("Location", "adsh"); ?></h3>
                <p><strong><?php _e("Address", 'adsh'); ?>:</strong>&ensp;<?php echo get_option('address'); ?></p>
                <p><strong><?php _e("Phone", 'adsh'); ?>:</strong>&ensp;<?php echo get_option('phone_1'); ?></p>
                <p><strong><?php _e("Fax", 'adsh'); ?>:</strong>&ensp;<?php echo get_option('fax'); ?></p>
                <p><strong><?php _e("E-mail", 'adsh'); ?>:</strong>&ensp;<a href="mailto:<?php echo get_option('email'); ?>"><?php echo get_option('email'); ?></a></p>
            </div>
            <div class="adsh-col-1-3 ta-r">
                <h3><?php _e("Social media", "adsh"); ?></h3>
                <ul class="socials-list ls-n mr-0">
                    <li class="social-item"><a href="<?php echo get_option('youtube_link'); ?>"><i class="fab fa-youtube-square"></i></a></li>
                    <li class="social-item"><a href="<?php echo get_option('instagram_link'); ?>"><i class="fab fa-instagram-square"></i></a></li>
                    <li class="social-item"><a href="<?php echo get_option('fb_link'); ?>"><i class="fab fa-facebook-square"></i></a></li>
                    <li class="social-item"><a href="<?php echo get_option('twitter_link'); ?>"><i class="fab fa-twitter-square"></i></a></li>
                    <li class="social-item"><a href="<?php echo get_option('vimeo_link'); ?>"><i class="fab fa-vimeo-square"></i></a></li>
                </ul>
            </div>
		</div><!-- .col-full -->
        <div class="bottom-line">
            <p>Copyright © <?php echo date("Y"); ?> RAUTENBERG MEDIA KG  •  
            <a href="<?php echo get_option('conditions_link'); ?>"><?php _e("Conditions", 'adsh'); ?></a>  •  
            <a href="<?php echo get_option('imprint_link'); ?>"><?php _e("Imprint", 'adsh'); ?></a>  •  
            <a href="<?php echo get_option('data_protection_link'); ?>"><?php _e("Data protection", 'adsh'); ?></a>  •  
            <a href="<?php echo get_option('withdrawal_link'); ?>"><?php _e("Right of withdrawal", 'adsh'); ?></a> 
            </p>
        </div><!-- .col-full -->
	</footer><!-- #colophon -->

	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
