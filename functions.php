<?php
/**
 * Adshop engine room
 *
 */
/**
 * Assign the Storefront version to a var
 */
$theme = wp_get_theme( 'adshop' );
$adshop_version = $theme['Version'];

$adshop = (object) array(
    'version'    => $adshop_version,
    
    /**
     * Initialize all the adshop things.
     */
    'main' => require_once 'inc/class-adshop.php',
    'customizer' => require 'inc/customizer/class-adshop-customizer.php',
);

require_once 'inc/storefront-template-hooks.php';
require_once 'inc/storefront-template-functions.php';

require_once 'inc/adshop-functions.php';
require_once 'inc/adshop-hooks.php';

