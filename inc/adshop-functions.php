<?php
/**
 * Adshop functions.
 *
 */

if ( ! function_exists( 'adshop_show_top_banner' ) ) {
    /**
     * Display top banner
     */
    function adshop_show_top_banner() {
        get_template_part( 'template-parts/header', 'banner' );
    }
}

if ( ! function_exists( 'show_optional_block' ) ) {
    /**
     * Display Optional block after wc products loop
     */
    function show_optional_block() {
        get_template_part( 'template-parts/block', 'optional' );
    }
}
