<?php
/**
 * Adsop hooks
 *
 */

/**
 * General
 *
 */
require_once 'hooks/general.php';

/**
 * Header
 *
 */

// cleaning
add_action( 'init', function(){
    remove_action( 'storefront_header', 'storefront_skip_links', 5 );
    remove_action( 'storefront_header', 'storefront_secondary_navigation', 30 );
    remove_action( 'storefront_header', 'storefront_product_search', 40 );
    remove_action( 'storefront_header', 'storefront_header_container_close', 41 );
    
    remove_action( 'storefront_header', 'storefront_primary_navigation_wrapper', 42 );
    remove_action( 'storefront_header', 'storefront_primary_navigation_wrapper_close', 68 );
    
    add_action( 'storefront_header', 'storefront_product_search', 65 );
    add_action( 'storefront_header', 'storefront_header_container_close', 70 );
    
    add_action( 'storefront_header', 'storefront_header_container', 80 );
    add_action( 'storefront_header', 'storefront_handheld_navigation', 90 );
    add_action( 'storefront_header', 'storefront_header_container_close', 100 );
    
    remove_action( 'storefront_page', 'storefront_page_header', 10 );
    add_action( 'storefront_page', 'adshop_article_header', 10 );
    add_action( 'storefront_header', 'adshop_show_top_banner', 110 );
    
    remove_action( 'storefront_before_content', 'woocommerce_breadcrumb', 10 );
    /* for shop page */
    remove_action( 'woocommerce_before_shop_loop', 'storefront_sorting_wrapper', 9 );
    remove_action( 'woocommerce_before_shop_loop', 'storefront_sorting_wrapper_close', 31 );
    // product card in list
    // remove "Sale" flash
    remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 6 );
    // remove " Showing all XX results"
    remove_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count', 20 );
    
    // show Optional block after wc products loop
    add_action( 'woocommerce_after_shop_loop', 'show_optional_block', 40 );
    
    /*  For Product page */
    // remove "Sale" flash
    
    
}, 10 );

/**
 * Footer
 *
 */

/**
 * Homepage
 *
 */

/**
 * Posts
 *
 */

/**
 * Pages
 *
 */

/**
 * Homepage Page Template
 *
 */
