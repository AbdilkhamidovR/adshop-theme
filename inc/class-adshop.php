<?php
/**
 * Adshop Class
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Adshop' ) ) :

	/**
	 * The main Adshop class
	 */
	class Adshop_theme {
        
        static $css_version = "1.0.16";
        static $js_version = "1.0.0";

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			add_action( 'after_setup_theme', array( $this, 'setup' ) );
            add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_front_scripts' ), 40 );
            add_action( 'admin_enqueue_scripts', array($this, 'enqueue_admin_scripts'), 20 );
            add_action( 'enqueue_block_editor_assets', array($this, 'enqueue_block_editor_assets'), 10 );
            
		}

		/**
		 * Sets up theme defaults and registers support for various WordPress features.
		 *
		 * Note that this function is hooked into the after_setup_theme hook, which
		 * runs before the init hook. The init hook is too late for some features, such
		 * as indicating support for post thumbnails.
		 */
		public function setup() {
			/*
			 * Load Localisation files.
			 */
            load_theme_textdomain( 'adsh', get_stylesheet_directory() . "/languages" ); // it works
		}
        
        /**
         * Enqueue front scripts and styles.
         *
         */
        public function enqueue_front_scripts() {
            /**
             * Styles
             */
            wp_enqueue_style( 'adshop-theme', get_stylesheet_directory_uri() . '/assets/css/style.min.css', '', self::$css_version );
            wp_style_add_data( 'adshop-theme', 'rtl', 'replace' );
    
            /**
             * Scripts
             */
            wp_enqueue_script( 'adshop-theme', get_stylesheet_directory_uri() . '/assets/js/front.js', array(), self::$js_version, true );
        }
        
        /**
         * Enqueue admin scripts and styles.
         *
         */
        public function enqueue_admin_scripts() {
            /**
             * Styles
             */
            wp_enqueue_style( 'adshop-theme-admin', get_stylesheet_directory_uri() . '/assets/css/admin-style.min.css', '', self::$css_version );
            wp_style_add_data( 'adshop-theme-admin', 'rtl', 'replace' );
    
            /**
             * Scripts
             */
            wp_enqueue_script( 'adshop-theme-admin', get_stylesheet_directory_uri() . '/assets/js/admin.js', array(), self::$js_version, true );
        }
        
        /**
         * Enqueue scripts and styles for gutteberg editor.
         *
         */
        public function enqueue_block_editor_assets() {
            /**
             * Styles
             */
            wp_enqueue_style( 'adshop-theme-gblocks', get_stylesheet_directory_uri() . '/assets/css/guttenberg-style.min.css', '', self::$css_version );
        }
	}
endif;

return new Adshop_theme();
