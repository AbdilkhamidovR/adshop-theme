<?php
/**
 * Adshop_Customizer Customizer Class extends Storefront Customizer Class
 *
 * @package  adshop
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Adshop_Customizer' ) ) :

	/**
	 * The Storefront Customizer class
	 */
	class Adshop_Customizer {

		/**
		 * Setup class.
		 */
		public function __construct() {
			add_action( 'customize_register', array( $this, 'customize_register' ), 10 );
		}
        
        public function customize_register( $wp_customize ) {
		    
            /**
             * Add Company info Section
             */
            
            $wp_customize->add_section(
                'adshop_info',
                array(
                    'title'    => __( 'Company info', 'adsh' ),
                    'priority' => 20,
                )
            );
    
            $this->add_email($wp_customize, 'adshop_info');
            $this->add_address($wp_customize, 'adshop_info');
            $this->add_phone_fax($wp_customize, 'adshop_info');
            $this->add_socials($wp_customize, 'adshop_info');
            $this->add_contracts($wp_customize, 'adshop_info');
            
        }
        
        
        protected function add_email( $wp_customize, $section_name ){
            /**
             * email
             */
            $wp_customize->add_setting(
                'email',
                array(
                    'default' => '',
                    'type' => 'option',
                    'sanitize_callback' => 'sanitize_email',
                )
            );
            $wp_customize->add_control(
                new WP_Customize_Color_Control(
                    $wp_customize,
                    'email',
                    array(
                        'label'    => __( 'Email', 'adsh' ),
                        'section'  => $section_name,
                        'settings' => 'email',
                        'priority' => 10,
                        'type' => 'text'
                    )
                )
            );
        }
        
        protected function add_address( $wp_customize, $section_name ){
            /**
             * Address
             */
            $wp_customize->add_setting(
                'address',
                array(
                    'default' => '',
                    'type' => 'option',
                    'sanitize_callback' => 'sanitize_textarea_field',
                )
            );
            $wp_customize->add_control(
                new WP_Customize_Color_Control(
                    $wp_customize,
                    'address',
                    array(
                        'label'    => __( 'Address', 'adsh' ),
                        'section'  => $section_name,
                        'settings' => 'address',
                        'priority' => 10,
                        'type' => 'text'
                    )
                )
            );
        }
        
        protected function add_phone_fax( $wp_customize, $section_name ){
            /**
             * Phone
             */
            $wp_customize->add_setting(
                'phone_1',
                array(
                    'default' => '',
                    'type' => 'option',
                    'sanitize_callback' => 'sanitize_textarea_field',
                )
            );
            $wp_customize->add_control(
                new WP_Customize_Color_Control(
                    $wp_customize,
                    'phone_1',
                    array(
                        'label'    => __( 'Phone', 'adsh' ),
                        'section'  => $section_name,
                        'settings' => 'phone_1',
                        'priority' => 10,
                        'type' => 'text'
                    )
                )
            );
            
            /**
             * Fax
             */
            $wp_customize->add_setting(
                'fax',
                array(
                    'default' => '',
                    'type' => 'option',
                    'sanitize_callback' => 'sanitize_textarea_field',
                )
            );
            $wp_customize->add_control(
                new WP_Customize_Color_Control(
                    $wp_customize,
                    'fax',
                    array(
                        'label'    => __( 'Fax', 'adsh' ),
                        'section'  => $section_name,
                        'settings' => 'fax',
                        'priority' => 10,
                        'type' => 'text'
                    )
                )
            );
		    
        }
        
        protected function add_socials( $wp_customize, $section_name ){
    
            /**
             * youtube
             */
            $wp_customize->add_setting(
                'youtube_link',
                array(
                    'default' => '',
                    'type' => 'option',
                    'sanitize_callback' => 'esc_url',
                )
            );
            $wp_customize->add_control(
                new WP_Customize_Color_Control(
                    $wp_customize,
                    'youtube_link',
                    array(
                        'label'    => __( 'Youtube', 'adsh' ),
                        'section'  => $section_name,
                        'settings' => 'youtube_link',
                        'priority' => 10,
                        'type' => 'text'
                    )
                )
            );
    
            /**
             * Instagram
             */
            $wp_customize->add_setting(
                'instagram_link',
                array(
                    'default' => '',
                    'type' => 'option',
                    'sanitize_callback' => 'esc_url',
                )
            );
            $wp_customize->add_control(
                new WP_Customize_Color_Control(
                    $wp_customize,
                    'instagram_link',
                    array(
                        'label'    => __( 'Instagram', 'adsh' ),
                        'section'  => $section_name,
                        'settings' => 'instagram_link',
                        'priority' => 10,
                        'type' => 'text'
                    )
                )
            );
    
            /**
             * fb
             */
            $wp_customize->add_setting(
                'fb_link',
                array(
                    'default' => '',
                    'type' => 'option',
                    'sanitize_callback' => 'esc_url',
                )
            );
            $wp_customize->add_control(
                new WP_Customize_Color_Control(
                    $wp_customize,
                    'fb_link',
                    array(
                        'label'    => __( 'Facebook', 'adsh' ),
                        'section'  => $section_name,
                        'settings' => 'fb_link',
                        'priority' => 10,
                        'type' => 'text'
                    )
                )
            );
    
            /**
             * Twitter
             */
            $wp_customize->add_setting(
                'twitter_link',
                array(
                    'default' => '',
                    'type' => 'option',
                    'sanitize_callback' => 'esc_url',
                )
            );
            $wp_customize->add_control(
                new WP_Customize_Color_Control(
                    $wp_customize,
                    'twitter_link',
                    array(
                        'label'    => __( 'Twitter', 'adsh' ),
                        'section'  => $section_name,
                        'settings' => 'twitter_link',
                        'priority' => 10,
                        'type' => 'text'
                    )
                )
            );
    
            /**
             * Vimeo
             */
            $wp_customize->add_setting(
                'vimeo_link',
                array(
                    'default' => '',
                    'type' => 'option',
                    'sanitize_callback' => 'esc_url',
                )
            );
            $wp_customize->add_control(
                new WP_Customize_Color_Control(
                    $wp_customize,
                    'vimeo_link',
                    array(
                        'label'    => __( 'Vimeo', 'adsh' ),
                        'section'  => $section_name,
                        'settings' => 'vimeo_link',
                        'priority' => 10,
                        'type' => 'text'
                    )
                )
            );
		    
        }

        protected function add_contracts( $wp_customize, $section_name ){
    
            /**
             * conditions
             */
            $wp_customize->add_setting(
                'conditions_link',
                array(
                    'default' => '',
                    'type' => 'option',
                    'sanitize_callback' => 'esc_url',
                )
            );
            $wp_customize->add_control(
                new WP_Customize_Color_Control(
                    $wp_customize,
                    'conditions_link',
                    array(
                        'label'    => __( 'Conditions', 'adsh' ),
                        'section'  => $section_name,
                        'settings' => 'conditions_link',
                        'priority' => 10,
                        'type' => 'text'
                    )
                )
            );
    
            /**
             * imprint
             */
            $wp_customize->add_setting(
                'imprint_link',
                array(
                    'default' => '',
                    'type' => 'option',
                    'sanitize_callback' => 'esc_url',
                )
            );
            $wp_customize->add_control(
                new WP_Customize_Color_Control(
                    $wp_customize,
                    'imprint_link',
                    array(
                        'label'    => __( 'Imprint', 'adsh' ),
                        'section'  => $section_name,
                        'settings' => 'imprint_link',
                        'priority' => 10,
                        'type' => 'text'
                    )
                )
            );
    
            /**
             * data_protection
             */
            $wp_customize->add_setting(
                'data_protection_link',
                array(
                    'default' => '',
                    'type' => 'option',
                    'sanitize_callback' => 'esc_url',
                )
            );
            $wp_customize->add_control(
                new WP_Customize_Color_Control(
                    $wp_customize,
                    'data_protection_link',
                    array(
                        'label'    => __( 'Data protection', 'adsh' ),
                        'section'  => $section_name,
                        'settings' => 'data_protection_link',
                        'priority' => 10,
                        'type' => 'text'
                    )
                )
            );
    
            /**
             * withdrawal
             */
            $wp_customize->add_setting(
                'withdrawal_link',
                array(
                    'default' => '',
                    'type' => 'option',
                    'sanitize_callback' => 'esc_url',
                )
            );
            $wp_customize->add_control(
                new WP_Customize_Color_Control(
                    $wp_customize,
                    'withdrawal_link',
                    array(
                        'label'    => __( 'Right of withdrawal', 'adsh' ),
                        'section'  => $section_name,
                        'settings' => 'withdrawal_link',
                        'priority' => 10,
                        'type' => 'text'
                    )
                )
            );
    
        }

        

	}

endif;

return new Adshop_Customizer();
