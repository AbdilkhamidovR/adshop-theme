<?php
/**
 * Extend Storefront template functions.
 *
 * @package adshop
 */

if ( ! function_exists( 'storefront_header_container' ) ) {
	/**
	 * The header container
	 */
	function storefront_header_container() {
		echo '<div class="col-full d-flex va-bm">';
	}
}


if ( ! function_exists( 'storefront_primary_navigation' ) ) {
    /**
     * Display Primary Navigation
     *
     * @since  1.0.0
     * @return void
     */
    function storefront_primary_navigation() {
        ?>
        <nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Navigation', 'storefront' ); ?>">
            <button class="menu-toggle" aria-controls="site-navigation" aria-expanded="false"><span><?php echo esc_html( apply_filters( 'storefront_menu_toggle_text', __( 'Menu', 'storefront' ) ) ); ?></span></button>
            <?php
            wp_nav_menu(
                array(
                    'theme_location'  => 'primary',
                    'container_class' => 'primary-navigation',
                )
            );
            ?>
        </nav><!-- #site-navigation -->
        <?php
    }
}


if ( ! function_exists( 'storefront_handheld_navigation' ) ) {
    /**
     * Display Primary Navigation
     *
     * @since  1.0.0
     * @return void
     */
    function storefront_handheld_navigation() {
        ?>
        <nav id="site-navigation" class="mobile-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Mobile Navigation', 'storefront' ); ?>">
            <?php
            wp_nav_menu(
                array(
                    'theme_location'  => 'handheld',
                    'container_class' => 'handheld-navigation',
                )
            );
            ?>
        </nav><!-- #site-navigation -->
        <?php
    }
}


if ( ! function_exists( 'storefront_cart_link' ) ) {
    /**
     * Cart Link
     * Displayed a link to the cart including the number of items present and the cart total
     *
     * @return void
     * @since  1.0.0
     */
    function storefront_cart_link() {
        if ( ! storefront_woo_cart_available() ) {
            return;
        }
        ?>
        <a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'storefront' ); ?>">
            <?php /* translators: %d: number of items in cart */ ?>
            <?php // echo wp_kses_post( WC()->cart->get_cart_subtotal() ); ?>
            <i class="cart-icon fas fa-shopping-cart"></i>
            <?php $count = WC()->cart->get_cart_contents_count(); ?>
            <?php if (!empty($count)):?>
                <span class="count"><?php echo $count; ?></span>
            <?php endif?>
        </a>
        <?php
    }
}


/**
 * Title for inner template
 *
 */
if ( ! function_exists( 'adshop_article_header' ) ) {
    function adshop_article_header(){
        if(is_page_template('template-inner.php')){
            the_title( '<h2 class="entry-title page-title">', '</h2>' );
        }
    }
}
