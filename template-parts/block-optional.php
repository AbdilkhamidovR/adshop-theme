<?php
/**
 * The template of Optional block
 *
 * @package adshop
 */
?>

<?php
?>
<div class="cfx"></div>
<h3 class="caption-h3 with-arrows products-caption"><?php _e("Optional", "adsh"); ?></h3>
<div class="col-full adshop-optional-block">
    <div class="adsh-row">
        <div class="left-side adsh-col-1-2">
            <h4 class="caption-h4">Sie finden nicht die passende Anzeige?</h4>
            <div class="body-text">
                Sie haben noch weitere Optionen. Für einen geringen Aufpreis gestalten wir Ihre Anzeige individuell. Nehmen Sie zu uns Kontakt auf. Sie haben bereits ein Design oder eine Agentur hat Ihnen ein Design entworfen? Laden Sie das Design hier einfach hoch.
            </div>
            <a href="javascript:void(0);" class="button adsh-darkgrey-btn with-arrow">KONTAKTIEREN SIE UNS </a>
            <!--<img src="--><?php //echo get_stylesheet_directory_uri(); ?><!--/assets/images/ic-dbl-arr-small.svg" width="35" height="26" alt="">-->
        </div>
        <div class="right-side adsh-col-1-2 with-vert-sep">
            <a href="javascript:void(0);" class=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ic-cloud.png" width="217" height="180" alt="" /></a>
            <br>
            <a href="javascript:void(0);" class="button adsh-darkgrey-btn">LADEN IHR DESIGN HOCH</a>
        </div>
    </div>
    <div class="note-block">
        <h5 class="caption-h5">Hinweis bei eigenem Design:</h5>
        <p>Prüfen und optimieren der Anzeige gegen Aufpreis</p>
    </div>
</div>

