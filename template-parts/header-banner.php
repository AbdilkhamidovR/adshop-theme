<?php
/**
 * The template for big banner on top.
 *
 * @package adshop
 */
?>

<?php
$show_banner = !is_archive() && !is_product() && has_post_thumbnail();
?>

<div class="col-full pd-0 adshop-top-banner-block<?php echo $show_banner ? " with-post-thumbnail" : ""; ?>">
    <h1 class="caption-h1">Rautenberg media<strong>Anzeigenshop</strong></h1>
    <?php if ($show_banner):?>
        <figure>
            <?php the_post_thumbnail('full'); ?>
        </figure>
        <figure class="figured-element">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/figured_element.svg" width="1920" height="304" alt="">
        </figure>
    <?php endif; ?>
</div>
<?php if (!$show_banner):?>
    <hr class="col-full adshop-top-line white">
    <hr class="col-full adshop-top-line grey">
<?php endif; ?>

